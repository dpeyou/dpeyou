open Helpers;
open Types;

module App = {
  type state = {
    scrollTop_Blog: int,
    scrollTop_Gallery: int,
    scrollTop_Home: int,
    scrollTop_Misc: int,
  };

  type action =
    | SetScrollTop(page, int);

  [@react.component]
  let make = /*no props*/ () => {
    //set the current page using router api
    //https://reasonml.github.io/reason-react/docs/en/router
    let currentPage: page =
      ReasonReact.Router.useUrl() |> /*pattern-match url to a page*/ urlToPage;
    //-------------------------------
    //---------------start of reducer
    let (state, dispatch) =
      React.useReducer(
        (state, action) =>
          switch (action) {
          | SetScrollTop(page, scrollTop) =>
            switch (page) {
            | Blog => {...state, scrollTop_Blog: scrollTop}
            | Gallery => {...state, scrollTop_Gallery: scrollTop}
            | Home => {...state, scrollTop_Home: scrollTop}
            | Miscellaneous => {...state, scrollTop_Misc: scrollTop}
            | Error => state
            }
          },
        //initial state
        {
          scrollTop_Blog: 0,
          scrollTop_Gallery: 0,
          scrollTop_Home: 0,
          scrollTop_Misc: 0,
        },
      );
    //-------------------------------
    //----------------end of reducer

    //-----------------------------------------
    //-----------------------------------layout
    <div
      id="App"
      style={ReactDOMRe.Style.make(
        ~background=
          "linear-gradient(to bottom right, #302a25, #3a3032, #3f3538)",
        ~bottom="0",
        ~left="0",
        ~margin="0 auto",
        ~maxWidth="850px",
        ~overflow="hidden",
        ~position="absolute",
        ~right="0",
        ~top="0",
        (),
      )}>
      <Navigation />
      {/*get current url*/
       switch (currentPage) {
       | Home =>
         <Home
           onScroll={(scrollTop, ()) =>
             dispatch(SetScrollTop(Home, scrollTop))
           }
           scrollTop={state.scrollTop_Home}
         />
       | Blog =>
         <Blog
           onScroll={(scrollTop, ()) =>
             dispatch(SetScrollTop(Blog, scrollTop))
           }
           scrollTop={state.scrollTop_Blog}
         />
       | Gallery =>
         <Gallery
           onScroll={(scrollTop, ()) =>
             dispatch(SetScrollTop(Gallery, scrollTop))
           }
           scrollTop={state.scrollTop_Gallery}
         />
       | Miscellaneous =>
         <Misc
           onScroll={(scrollTop, ()) =>
             dispatch(SetScrollTop(Miscellaneous, scrollTop))
           }
           scrollTop={state.scrollTop_Misc}
         />
       | Error => React.null
       }}
      //scroll-to-top button
      <ScrollToTop
        currentPage
        display={
          (
            switch (currentPage) {
            | Blog => state.scrollTop_Blog
            | Home => state.scrollTop_Home
            | Gallery => state.scrollTop_Gallery
            | Miscellaneous => state.scrollTop_Misc
            | _anyOtherPage => 0 /* 0 < 600, therefore item won't display in this case*/
            }
          )
          > 600
            ? "block" : "none"
        }
      />
    </div>;
  };
};
/*render to html file at div with ID "app"*/
ReactDOMRe.renderToElementWithId(<App />, "AppContainer");

/*---------------------------------------end of App module*/
