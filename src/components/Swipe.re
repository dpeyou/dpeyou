//----------------------------------------------------
//use the "react-swipeable" library, installed via npm
module ReactSwipeable = {
  [@bs.module "react-swipeable"] [@react.component]
  external make:
    (
      ~nodeName: 'string,
      ~onSwipedDown: 'a => unit,
      ~onSwipedLeft: 'a => unit,
      ~onSwipedRight: 'a => unit,
      ~onSwipedUp: 'a => unit,
      ~children: React.element
    ) =>
    React.element =
    "Swipeable";
  /*
    ES6 equivalent:
      import { Swipeable } from "react-swipeable"
   */
};

//-----------------------------
//our swipe-accepting component
[@react.component]
let make = /*props*/
    (
      ~nodeName: string="div",
      ~onSwipedDown: 'a => unit=_ => (),
      ~onSwipedLeft: 'a => unit=_ => (),
      ~onSwipedRight: 'a => unit=_ => (),
      ~onSwipedUp: 'a => unit=_ => (),
      ~children: React.element,
    ) => {
  <ReactSwipeable nodeName onSwipedDown onSwipedLeft onSwipedRight onSwipedUp>
    children
  </ReactSwipeable>;
};
