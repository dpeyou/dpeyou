open Helpers;

[@react.component]
let make = /*~props*/
    (
      ~background: string="linear-gradient(to bottom, rgba(70,60,63,0.9), rgba(60,50,53, 0.9))",
      ~height: string="2.6rem",
      ~pageName: string,
      ~position: string="absolute",
    ) => {
  //------------------------
  //------------------layout
  <header
    id="Header"
    style={ReactDOMRe.Style.make(
      ~alignItems="center",
      ~background,
      ~boxShadow="rgb(38, 30, 29) 0px 0px 103px 20px",
      ~display="flex",
      ~height,
      ~left="0",
      ~justifyContent="space-between",
      ~padding="0 5%",
      ~position,
      ~right="0",
      ~top="0",
      ~zIndex="3",
      (),
    )}>
    //----
    //logo

      <Box fontFamily="ork" fontSize="1.275rem" letterSpacing="-1px" margin="0">
        <span className="yellowText"> {"dP" |> str} </span>
        <span className="redText"> {"ns" |> str} </span>
      </Box>
      //---------
      //page name
      <div style={ReactDOMRe.Style.make(~fontSize="1.35rem", ())}>
        {pageName |> str}
      </div>
    </header>;
};
