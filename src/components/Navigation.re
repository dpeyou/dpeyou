open Helpers;
open Images;
open Types;

type state = {
  isMenuOpened: bool,
  openMenuButton: string /*for infinite up & down movemoent of menu icon*/
};

type action =
  | Shift(direction)
  | ToggleMenu(bool);

let menuItems: array(menuItem) = [|
  {color: "lightpink", name: "home", page: Home},
  {color: "#fee", name: "blog", page: Blog},
  {color: "lightyellow", name: "gallery", page: Gallery},
  {color: "#ffe0a0", name: "misc", page: Miscellaneous},
|];

[@react.component]
let make = /*no props*/ () => {
  //------------------------------------
  //-------------------start of reducer
  let (state, dispatch) =
    React.useReducer(
      (state, action) =>
        switch (action) {
        | Shift(direction) =>
          direction == Down
            ? {...state, openMenuButton: "translateY(6px)"}
            : {...state, openMenuButton: "translateY(-6px)"}

        | ToggleMenu(isMenuOpened) =>
          isMenuOpened == true
            ? {...state, isMenuOpened: false}
            : {...state, isMenuOpened: true}
        },
      //initial state
      {isMenuOpened: false, openMenuButton: "translateY(-6px)"},
    );
  //------------------------------------
  //----------------------end of reducer

  //infinite animation of menu icon
  let _Animation =
    React.useState(() => {
      //---------------------------setting timeout
      //since this block is a timeout, it will only be executed once
      let _StartAnimation =
        Js.Global.setTimeout(() => dispatch(Shift(Down)), 1000);
      //----------------------------end of timeout
      //------------------------------------------
      //now set interval to make it loop...forever! harharhar!
      let _infinitLoop_ =
        Js.Global.setInterval(
          () => {
            let _Up = dispatch(Shift(Up));
            let _Down =
              Js.Global.setTimeout(
                //set timeout so that ShitDown is executed after ShiftUp
                () => dispatch(Shift(Down)),
                1100 /*button's transition set to ~1050ms*/,
              );
            ();
          },
          2250,
        );
      ();
    });

  //shorthand
  let isMenuOpened: bool = state.isMenuOpened;

  //--------------------------------------------
  //--------------------------------------layout
  <nav
    id="Navigation"
    style={ReactDOMRe.Style.make(
      ~display="flex",
      ~justifyContent="center",
      ~width="100%",
      (),
    )}>
    //----------------

      <Button
        bottom="0"
        id="Button_OpenMenu"
        onClick={() => dispatch(ToggleMenu(false))}
        /* onTouchMove={_touchEvent => {
             dispatch(ToggleMenu(false));
             Js.log(_touchEvent |> ReactEvent.Touch.touches);
           }}*/
        padding="0"
        position="absolute"
        transform={isMenuOpened ? "translateY(30%)" : "translateY(0%)"}
        transition="transform 700ms cubic-bezier(0.08, 0.68, 0, 0.85)"
        zIndex="2">
        <Swipe
          onSwipedUp={_event =>
            //open menu
            dispatch(ToggleMenu(false))}>
          <img
            alt="open_menu"
            src=chevronUp
            style={ReactDOMRe.Style.make(
              ~background="rgba(67, 56, 59, 0.50)",
              ~borderRadius="50px",
              ~boxShadow="0 0 60px 0 #483f40",
              ~transform=state.openMenuButton,
              ~transition="transform 1050ms ease-out",
              ~width="5rem",
              (),
            )}
          />
        </Swipe>
      </Button>
      //------------
      //menu element
      <ul
        id="Container_Menu"
        style={ReactDOMRe.Style.make(
          ~background=
            "linear-gradient(to top left, #332a2d, #3a3032, #4a4043)",
          ~bottom="0",
          ~display="flex",
          ~flexDirection="column",
          ~fontFamily="cagli",
          ~height="100vh",
          ~justifyContent="flex-end",
          ~listStyle="none",
          ~margin="0",
          ~opacity={isMenuOpened ? "0.9" : "0"},
          ~padding="0",
          ~pointerEvents={isMenuOpened ? "auto" : "none"},
          ~position="absolute",
          ~textAlign="center",
          ~transform={isMenuOpened ? "translateY(0px)" : "translateY(10px)"},
          ~transition="450ms cubic-bezier(0.08, 0.68, 0, 0.85)",
          ~width="100%",
          ~zIndex={isMenuOpened ? "3" : "1"},
          (),
        )}>
        <Swipe
          onSwipedDown={_event =>
            //close menu
            dispatch(ToggleMenu(true))}>
          {menuItems
           |> Array.map((menuItem: menuItem) =>
                <li key={menuItem.name}>
                  <Button
                    background="none"
                    className="menuItem"
                    color={menuItem.color}
                    fontSize="3.75rem"
                    onClick={() =>
                      isMenuOpened
                        ? {
                          /*close menu*/ dispatch(ToggleMenu(true));
                          /*update url*/ ReasonReact.Router.push(
                            "#" ++ menuItem.name,
                          );
                        }
                        : /*do nothing*/ ()
                    }
                    pointerEvents={isMenuOpened ? "auto" : "none"}
                    tabIndex={isMenuOpened ? /*can tab*/ 0 : /*no tab*/ (-1)}
                    width="100%">
                    {menuItem.name |> str}
                  </Button>
                </li>
              )
           |> /*convert array to react element*/ React.array}
          //-----------------
          //close-menu button
          <li
            style={ReactDOMRe.Style.make(
              ~display="flex",
              ~justifyContent="center",
              (),
            )}>
            <Button
              id="Button_CloseMenu"
              onClick={() =>
                isMenuOpened ? dispatch(ToggleMenu(true)) : /*do nothing*/ ()
              }
              pointerEvents={isMenuOpened ? "auto" : "none"}
              tabIndex={isMenuOpened ? /*can tab*/ 0 : /*no tab*/ (-1)}>
              <img
                alt="close_menu"
                src=chevronX
                style={ReactDOMRe.Style.make(
                  ~padding="0.5rem 2rem 2rem",
                  ~pointerEvents="none",
                  ~width="4.25rem",
                  (),
                )}
              />
            </Button>
          </li>
        </Swipe>
      </ul>
    </nav>;
  //open-menu button
};
