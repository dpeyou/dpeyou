open Helpers;
open Images;

[@react.component]
let make = /*props*/
    (
      ~background: string="none",
      ~boxShadow: string="0 0 103px 3px #1e0a0c",
      ~display: string="flex",
      ~height: string="",
      ~flexDirection: string="column",
      ~fontFamily: string="",
      ~id: string="",
      ~padding: string="0 6%",
      ~position: string="absolute",
      ~children: React.element=React.null,
    ) => {
  //----------------------------
  //----------------------layout
  <footer
    className="footer"
    id
    style={ReactDOMRe.Style.make(
      ~alignItems="center",
      ~background,
      ~bottom="0",
      ~boxShadow,
      ~display,
      ~flexDirection,
      ~fontFamily,
      ~height,
      ~left="0",
      ~padding,
      ~position,
      ~right="0",
      ~transition="0s",
      ~zIndex="1",
      (),
    )}>
    <Box className="socialMediaIcons">
      <a rel="me" href="https://mastodon.social/@dpeyou" target="_blank">
        <img
          alt="mastodon_button"
          src=at_sign
          style={ReactDOMRe.Style.make(~padding="1.5rem", ())}
        />
      </a>
      <a href="https://gitlab.com/dpeyou" target="_blank">
        <img
          alt="gitlab_button"
          src=gitlabIcon
          style={ReactDOMRe.Style.make(~padding="1.5rem", ())}
        />
      </a>
    </Box>
    <p
      style={ReactDOMRe.Style.make(
        ~padding="0 1rem",
        ~textAlign="center",
        (),
      )}>
      {"Site crafted from my Ubuntu Linux, somewhere in this world" |> str}
    </p>
    <a
      href="https://gitlab.com/dpeyou/dpeyou"
      style={ReactDOMRe.Style.make(
        ~background=
          "linear-gradient(to top, rgb(70, 60, 63), rgb(80, 70, 63))",
        ~borderRadius="5%",
        ~color="#dfd",
        ~margin="2rem 0 0",
        ~padding="0.8rem",
        ~textDecoration="none",
        (),
      )}
      target="_blank">
      {"View this website's source code" |> str}
    </a>
    <p> {{js|© |js} ++ {|2019|} ++ " D2C Labs" |> str} </p>
    <a
      href="https://en.wikipedia.org/wiki/Cameroon"
      style={ReactDOMRe.Style.make(~margin="3.25rem 0 0", ())}
      target="_blank">
      <img
        alt="flag_of_cameroon"
        src=cameroon
        style={ReactDOMRe.Style.make(~padding="1.5rem", ())}
      />
    </a>
    children
  </footer>;
};
