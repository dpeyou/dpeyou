open Helpers;
open Images;
open Types;

[@react.component]
let make = (~closePost: unit => unit, ~currentPost: post) => {
  //shorthand
  let post: post = currentPost;

  //------------------------------
  //------------------------layout
  <>
    <Header pageName={"Reading: " ++ currentPost.title} />
    <ScrollView
      background="linear-gradient(to left top, rgb(51, 42, 45), rgb(58, 48, 50), rgb(74, 64, 67))"
      id="ScrollView_Reader">
      <main
        style={ReactDOMRe.Style.make(
          ~lineHeight="1.65",
          ~padding="2.5rem 1.5rem 10rem",
          (),
        )}>
        <h1
          className="postTitle"
          style={ReactDOMRe.Style.make(
            ~color="#ffe0a0",
            ~fontWeight="400",
            ~margin="0",
            (),
          )}>
          {post.title |> str}
        </h1>
        //----------------------------
        //dates...dates are yummy, lol
        <Box
          alignItems="flex-start"
          border="dashed #ffe0a0"
          borderWidth="0 0 3px"
          color="lightpink"
          flexDirection="column"
          margin="0 0 1.5rem"
          padding="0 1.2rem 1.2rem">
          <div> {"Posted: " ++ post.date |> str} </div>
          <div> {"Last update: " ++ post.lastUpdate |> str} </div>
        </Box>
        {post.content}
      </main>
    </ScrollView>
    //-----------------
    //close-post button
    <Button
      background="#433"
      border="dashed #ffe0a0"
      borderRadius="5%"
      borderWidth="3px"
      bottom="6rem"
      boxShadow="rgb(42, 34, 35) 0px 0px 10px 5px"
      className="button_ClosePost"
      left="2rem"
      onClick=closePost
      padding="0.5rem"
      position="absolute"
      zIndex="2">
      <img src=chevronX style={ReactDOMRe.Style.make(~width="2.2rem", ())} />
    </Button>
  </>;
};
