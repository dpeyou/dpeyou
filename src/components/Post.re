open Helpers;

[@react.component]
let make =
    (
      ~clickPost: unit => unit,
      ~content: React.element,
      ~date: string="",
      ~id: string,
      ~lastUpdate: string="",
      ~title: string,
    ) => {
  //-----------------------
  //-----------------layout
  <article
    className="post"
    id
    style={ReactDOMRe.Style.make(
      ~display="flex",
      ~flexDirection="column",
      ~marginBottom="2.5rem",
      ~transition="300ms",
      (),
    )}>
    <h1
      className="postTitle"
      style={ReactDOMRe.Style.make(
        ~color="#ffe0a0",
        ~fontWeight="400",
        ~margin="0",
        (),
      )}>
      {title |> str}
    </h1>
    <Box
      alignItems="flex-start"
      color="lightpink"
      flexDirection="column"
      margin="0"
      padding="0.75rem 1.2rem 1.3rem">
      <div> {"Posted: " ++ date |> str} </div>
      <div> {"Last update: " ++ lastUpdate |> str} </div>
    </Box>
    <p
      className="postContent"
      style={ReactDOMRe.Style.make(
        ~borderTop="dashed 3px #ffe0a0",
        //~fontSize="1.1rem",
        ~height="6rem",
        ~lineHeight="1.65",
        ~margin="0",
        ~overflow="hidden",
        ~paddingTop="1rem",
        (),
      )}>
      content
    </p>
    <Button
      background="linear-gradient(to top, rgb(70, 60, 63), rgb(80, 70, 63))"
      className="button_OpenPost"
      color="#fee"
      margin="0.75rem 0"
      onClick=clickPost
      padding="0.75rem 0.5rem">
      {"Read post" |> str}
    </Button>
  </article>;
};
