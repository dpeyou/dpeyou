open Helpers;
open Types;

[@react.component]
let make = (~currentPage: page, ~display: string="none") => {
  //------------------
  //------------layout
  <Button
    background="#433"
    border="dashed #ffe0a0"
    borderWidth="3px"
    borderRadius="5%"
    bottom="6rem"
    boxShadow="rgb(42, 34, 35) 0px 0px 10px 5px"
    color="#eee"
    display
    fontSize="1.3rem"
    id="Button_ScrollToTop"
    onClick={_ =>
      scrollToTop(
        /*switch the target id based on the current page/route*/
        switch (currentPage) {
        | Blog => "ScrollView_Blog"
        | Error => "ScrollView_Error"
        | Gallery => "ScrollView_Gallery"
        | Home => "ScrollView_Home"
        | Miscellaneous => "ScrollView_misc"
        },
      )
    }
    padding="0.8rem"
    position="absolute"
    right="5%"
    zIndex="1">
    {"top" |> React.string}
  </Button>;
};
