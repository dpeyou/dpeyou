module Action = {
  let getScrollTop: string => int =
    _id => [%bs.raw {|
    document.getElementById(_id).scrollTop
  |}];

  let setScrollTop: (int, string) => unit =
    (_scrollTop, _id) => [%bs.raw
      {|
      document.getElementById(_id).scrollTop = _scrollTop
    |}
    ];
};

type state = {
  scrollTop: int /*capture how much the div (window for iOS if not mistaken) has been scrolled using document.element.scrollTop*/,
  transform: string /*for little animation of the component as it renders*/,
};

type action =
  | Animate /*little animation for when the component renders*/
  | GetScrollTop(/*targets document.element.scrollTop*/ int);

[@react.component]
let make = /*props*/
    (
      ~background: string="",
      ~display: string="block",
      ~id: string /*REQUIRE the id: an empty id returns error, due to presence of "document.getElementById" in functions above*/,
      ~marginBottom: string="0rem" /*to account for height of fixed footer. Set to zero for pages that don't have a fixed footer*/,
      ~onScroll: (int, unit) => unit=(_, ()) => () /*scroll the page on re-render*/,
      // ~tabIndex: int=0,
      ~position: string="absolute",
      ~scrollTop: int=0 /*get value of scrollTop when re-rendering page(component) from parent component (<App />)*/,
      ~transition: string={| 500ms cubic-bezier(0.08, 0.68, 0, 0.85), z-index 0s |},
      ~zIndex: string="1",
      ~children: React.element,
    ) => {
  //----------------------------------
  //------------------start of reducer
  let (state, dispatch) =
    React.useReducer(
      (state, action) =>
        switch (action) {
        | Animate => {...state, transform: "scale(1.0)"}
        | GetScrollTop(scrollTop) => {...state, scrollTop}
        },
      //initial state
      {scrollTop, transform: "scale(1.05)"},
    );
  //---------------------------------
  //-------------------end of reducer

  let _onRender =
    React.useEffect0(() => {
      /*scroll the page by getting the initial scrollTop value from the scrollTop prop*/
      Action.setScrollTop(state.scrollTop, id);
      dispatch(Animate);
      None;
    });

  //------------------------------------
  //------------------------------layout
  <div
    className="scrollview scrollView"
    id
    onScroll={_ =>
      Action.getScrollTop(id)
      |> (
        scrollTopValue => {
          ()
          /*use prop to send the scrollTop value to parent component (<App />) & save it there*/
          |> onScroll(scrollTopValue);
        }
      )
    }
    style={ReactDOMRe.Style.make(
      ~background,
      ~bottom="0",
      ~boxShadow="inset rgb(16, 15, 34) 0px -150px 200px -200px",
      ~display,
      ~justifyContent="center",
      ~left="0",
      ~marginBottom,
      ~overflowX="hidden",
      ~overflowY="scroll",
      ~paddingTop="2.7rem" /*should correspond with height of <Header />*/,
      ~position,
      ~right="0",
      ~top="0",
      ~transform=state.transform,
      ~transition,
      ~width="100%",
      ~zIndex,
      (),
    )}>
    children
  </div>;
};
