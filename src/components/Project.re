open Helpers;
open Images;
open Types;

[@react.component]
let make = /*props*/
    (
      ~description: string,
      ~onImgClick:
         (
           array(screenShot) /*capture images*/,
           int /*capture index of the image that was clicked*/,
           ReactEvent.Mouse.t
         ) =>
         unit,
      ~name: string,
      ~screenShots: array(screenShot),
      ~sourceCode: string,
      ~url: string,
    ) => {
  //------------------
  //------------layout
  <article
    className="project"
    style={ReactDOMRe.Style.make(
      ~borderTop="2px solid #ffe0a0",
      ~boxShadow="#211 0px -100px 100px -100px",
      ~marginBottom="5rem",
      ~paddingTop="0.3rem",
      (),
    )}>
    <h1
      className="projectTitle yellowText"
      style={ReactDOMRe.Style.make(~marginBottom="0.5rem", ())}>
      {name |> str}
    </h1>
    <p
      className="projectContent"
      style={ReactDOMRe.Style.make(~marginTop="0.5rem", ())}>
      {description |> str}
    </p>
    <Box
      border="dashed #ffe0a0"
      borderWidth="2px 0 0"
      boxShadow="rgb(28, 18, 15) 0px 0px 100px -30px"
      className="slideShow"
      justifyContent="flex-start"
      margin="1.5rem 0 1.5rem"
      overflowX="scroll">
      {screenShots
       |> Array.mapi((index, image) =>
            <img
              alt={image.alt}
              key={image.alt}
              onClick={onImgClick(
                screenShots,
                index /*get the index of the image that was clicked*/,
              )}
              src={image.src}
              style={ReactDOMRe.Style.make(
                ~border="solid #322",
                ~borderWidth="0 1px",
                ~height="8rem",
                (),
              )}
            />
          )
       |> React.array}
    </Box>
    <Box className="links" justifyContent="space-between">
      {sourceCode == ""
         ? <p
             className="greenText"
             style={ReactDOMRe.Style.make(~width="50%", ())}>
             {"Code not available yet" |> str}
           </p>
         : <a
             className="sourceCode link"
             href=sourceCode
             style={ReactDOMRe.Style.make(
               ~background=
                 "linear-gradient(to top, rgb(70, 60, 63), rgb(80, 70, 63))",
               ~borderRadius="1px",
               ~color="#fee",
               ~display="flex",
               ~fontSize="1.25rem",
               ~padding="0.8rem",
               ~textDecoration="none",
               ~whiteSpace="pre",
               (),
             )}
             target="_blank">
             {"Code " |> str}
             <img
               alt="gitlab_icon"
               src=gitlabIcon
               style={ReactDOMRe.Style.make(~width="1.0rem", ())}
             />
           </a>}
      {url == ""
         ? <p
             style={ReactDOMRe.Style.make(
               ~color="#fca326" /*gitlab orange*/,
               ~textAlign="right",
               ~width="50%",
               (),
             )}>
             {"Request for access to demo" |> str}
           </p>
         : <a
             className="demo link"
             href=url
             style={ReactDOMRe.Style.make(
               ~background=
                 "linear-gradient(to top, rgb(70, 60, 63), rgb(80, 70, 63))",
               ~borderRadius="1px",
               ~color="#fee",
               ~display="flex",
               ~fontSize="1.25rem",
               ~padding="0.8rem",
               ~textDecoration="none",
               ~whiteSpace="pre",
               (),
             )}
             target="_blank">
             {"Demo " |> str}
             <img
               alt="external_link"
               src=extLink
               style={ReactDOMRe.Style.make(~width="1.15rem", ())}
             />
           </a>}
    </Box>
  </article>;
};
