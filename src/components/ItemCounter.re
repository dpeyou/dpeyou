open Helpers;

[@react.component]
let make = (~itemName: string, ~numberOfItems: string) => {
  //----------------
  //----------layout
  <div style={ReactDOMRe.Style.make(~fontFamily="Cagli", ~color="#fee", ())}>
    <div
      className="numberOfItems"
      style={ReactDOMRe.Style.make(
        ~color="#ffa0a0",
        ~display="inline-block",
        ~fontWeight="bold",
        ~paddingTop="0.1rem" /*for better looking vertical alignment*/,
        (),
      )}>
      {numberOfItems |> str}
    </div>
    {(
       switch (numberOfItems) {
       | "1" => /*add space*/ " " ++ itemName
       | _anyOtherNumber =>
         /*add space*/ " " ++ itemName ++ /*add letter 's'*/ "s"
       }
     )
     |> str}
  </div>;
};
