open Types;

let pageToString: page => string =
  page =>
    switch (page) {
    | Blog => "blog"
    | Error => "error"
    | Gallery => "gallery"
    | Home => "home"
    | Miscellaneous => "misc"
    };

//for scroll-to-top button
let scrollToTop: string => unit =
  _id => [%bs.raw {|
      document.getElementById(_id).scrollTop = 0
    |}];

let str: string => React.element = React.string;

let urlToPage: url => page =
  url =>
    switch (url.hash) {
    | "blog" => Blog
    | "gallery" => Gallery
    | ""
    | "home" => Home
    | "misc" => Miscellaneous
    | _ => Error
    };
