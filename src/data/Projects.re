open Images;
open Types;

let init: project = {
  description: "A starter-kit web app with the typical re-useable elements I need in ReasonML.",
  name: "template-1",
  screenShots: [|
    {alt: "screenShot1", src: bb1},
    {alt: "screenShot2", src: bb2},
    {alt: "screenShot3", src: bb3},
    {alt: "screenShot4", src: bb4},
  |],
  sourceCode: "https://gitlab.com/dpeyou/init-web.re",
  url: "https://init-web.herokuapp.com/",
};

let w: project = {
  description: "This app helps me keep up with the upcoming fixtures of select leagues (5-6), including the German Bundesliga & English Premier League.",
  name: "Mini sports app",
  screenShots: [|
    {alt: "screenShot1", src: aa2},
    {alt: "screenShot2", src: aa1},
    {alt: "screenShot3", src: aa3},
    {alt: "screenShot4", src: aa4},
    {alt: "screenShot5", src: aa5},
    {alt: "screenShot6", src: aa6},
    {alt: "screenShot7", src: aa7},
  |],
  sourceCode: "https://gitlab.com/dpeyou/w",
  url: "https://thedub.herokuapp.com/",
};

let testA: project = {
  description: "Note app which includes a news feed. Includes the ability to bookmark some articles for later.",
  name: "testA note app",
  screenShots: [|
    {alt: "testA_screenShot1", src: testAs1},
    {alt: "testA_screenShot2", src: testAs2},
    {alt: "testA_screenShot3", src: testAs3},
    {alt: "testA_screenShot4", src: testAs4},
  |],
  sourceCode: "", /*not yet...*/
  url: "" /*left this out on purpose lol!*/,
};

let projects: list(project) = [init, w, testA];
