open Helpers;
open Images;
open Types;

let introPost: post = {
  content:
    <>
      {"Hello World! Finally getting this blog thingy started. For me the hardest part about working on many apps is figuring out a good stopping point. At the risk of sounding repetetive, Im going to say it again: learning never stops! That means each time I learn something new, I feel silly about the way I was previously coding and frantically try to update everything. The process is always exciting... and lame af lol. Welcome to my blog y'all. I might also post in French and Portuguese. Spanish will require a little more time for me and Japansese... LOL! Another 2 years as I get busy. Here's a shot of me working on my site..."
       |> str}
      {<img
         src=post1shot1
         style={ReactDOMRe.Style.make(~margin="1rem 0", ~width="100%", ())}
       />}
    </>,
  date: "11th Nov '19",
  lastUpdate: "12th Nov '19",
  id: "post-1",
  title: "Intro post",
};

let posts: list(post) = [introPost];
