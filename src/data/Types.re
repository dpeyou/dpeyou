type direction =
  | Down
  | Left
  | Right
  | Up;

type menuItem = {
  color: string,
  name: string,
  page,
}
and page =
  | Blog
  | Error
  | Gallery
  | Home
  | Miscellaneous;

type post = {
  content: React.element,
  date: string,
  id: string,
  lastUpdate: string,
  title: string,
};

type project = {
  description: string,
  name: string,
  screenShots: array(screenShot),
  sourceCode: string,
  url: string,
}
and screenShot = {
  alt: string,
  src: string,
};

type url = ReasonReact.Router.url;
