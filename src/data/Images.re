//-------------
//gallery page
[@bs.module] external at_sign: string = "../img/at-sign.svg";
[@bs.module] external cameroon: string = "../img/cmr.png";
[@bs.module] external chevronX: string = "../img/chevronX.svg";
[@bs.module] external extLink: string = "../img/extLink.svg";
[@bs.module] external gitlabIcon: string = "../img/gitlab.svg";
[@bs.module] external leftArrow: string = "../img/left.svg";
[@bs.module] external rightArrow: string = "../img/right.svg";

//---------
//home page
[@bs.module] external at_sign: string = "../img/at-sign.svg";
[@bs.module] external cameroon: string = "../img/cmr.png";
[@bs.module] external gitlabIcon: string = "../img/gitlab.svg";

//-----------------------
//navigation: menu button
[@bs.module] external chevronUp: string = "../img/chevronUp.svg";
[@bs.module] external chevronX: string = "../img/chevronX.svg";

//------
//post 1
[@bs.module] external post1shot1: string = "../img/post1shot1.jpg";

//--------------------
//scroll-to-top button
[@bs.module] external arrow_up: string = "/img/arrow_up.svg";

//----------------------
//sports app screenshots
[@bs.module] external aa1: string = "../img/aa1.png";
[@bs.module] external aa2: string = "../img/aa2.png";
[@bs.module] external aa3: string = "../img/aa3.png";
[@bs.module] external aa4: string = "../img/aa4.png";
[@bs.module] external aa5: string = "../img/aa5.png";
[@bs.module] external aa6: string = "../img/aa6.png";
[@bs.module] external aa7: string = "../img/aa7.png";

//------------------------
//template app screenshots
[@bs.module] external bb1: string = "../img/bb1.png";
[@bs.module] external bb2: string = "../img/bb2.png";
[@bs.module] external bb3: string = "../img/bb3.png";
[@bs.module] external bb4: string = "../img/bb4.png";

//----------------------------
//testA - note app screenshots
[@bs.module] external testAs1: string = "../img/s1.png";
[@bs.module] external testAs2: string = "../img/s2.png";
[@bs.module] external testAs3: string = "../img/s3.png";
[@bs.module] external testAs4: string = "../img/s4.png";
