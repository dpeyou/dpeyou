open Helpers;

[@react.component]
let make = (~onScroll: (int, unit) => unit, ~scrollTop: int) => {
  //----------------------------
  //----------------------layout
  <div id="Page_Home">
    <Header pageName="home" />
    <ScrollView id="ScrollView_Home" onScroll scrollTop>
      <main
        style={ReactDOMRe.Style.make(
          ~margin="0 auto 10rem",
          ~maxWidth="700px",
          ~padding="0 5%",
          (),
        )}>
        <Box flexDirection="column" margin="2.9rem 0 4rem">
          <h1
            className="yellowText"
            style={ReactDOMRe.Style.make(
              ~fontSize="1.7rem",
              ~marginBottom="0.8rem",
              (),
            )}>
            {"Darren Peyou Ndi-Samba" |> str}
          </h1>
          <Box fontSize="1.2rem" margin="0" whiteSpace="pre">
            <span className="lightPinkText"> {"Web Developer" |> str} </span>
            <span className="separator"> {"  |  " |> str} </span>
            <span className="greenText"> {"Avocado lover" |> str} </span>
          </Box>
        </Box>
        <p className="shortBio">
          {"I'm a web developer who enjoys creating user-interfaces. The NodeJs ecosystem rocks & I forgot what life was like without ReactJs. My strength is on the front-end, but I enjoy every aspect of web development."
           |> str}
        </p>
        <p className="shortBio">
          {"I learned how to code using services like TeamTreehouse & FreeCodeCamp, by following random tutorials online (dev.to, tutorialspoint) and reading documentation until my eyes would close against my will."
           |> str}
        </p>
        <h2
          className="yellowText"
          style={ReactDOMRe.Style.make(~fontSize="1.5rem", ())}>
          {"My developer stack includes:" |> str}
        </h2>
        <ul style={ReactDOMRe.Style.make(~lineHeight="1.6", ())}>
          <li className="yellowText"> {"HTML & CSS" |> str} </li>
          <li className="lightGreenText"> {"NodeJS" |> str} </li>
          <li className="redText"> {"ReasonML" |> str} </li>
          <li className="lightBlueText">
            {"ReactJS & React-Native" |> str}
          </li>
          <li className="pinkText"> {"GraphQL" |> str} </li>
        </ul>
        //<li> {"Prisma" |> str} </li>
        <p>
          {"Other pieces of technology I wish to experiment with include VueJS, Rust, Scala & databases in general."
           |> str}
        </p>
        <h2
          className="yellowText"
          style={ReactDOMRe.Style.make(~fontSize="1.5rem", ())}>
          {"I'm a polyglot, language nerd" |> str}
        </h2>
        <ol
          style={ReactDOMRe.Style.make(
            ~fontSize="1rem",
            ~lineHeight="1.6",
            ~listStyle="square",
            (),
          )}>
          <li lang="fr-fr"> {{js|Je parle français|js} |> str} </li>
          <li lang="pt-br"> {{js|Falo português básico|js} |> str} </li>
          <li lang="es-mx"> {{js|Hablo un poco de español|js} |> str} </li>
          <li lang="ja-jp">
            {{js|日本語勉強しでいます (very basic Japanese)|js}
             |> str}
          </li>
        </ol>
        <p>
          {"Others I'm currently learning (or will soon start) include German, Russian & my African language: Ewondo. Once I can speak Ewondo beyond a couple of phrases, I'll be granted entrance into Wakanda."
           |> str}
        </p>
        <h3
          lang="fr-fr"
          style={ReactDOMRe.Style.make(
            ~margin="0 1rem 0 0",
            ~textAlign="right",
            (),
          )}>
          <span className="redText"> {"Fra" |> str} </span>
          <span className="whiteText"> {{js|nç|js} |> str} </span>
          <span className="blueText"> {"ais" |> str} </span>
        </h3>
        <p
          lang="fr-fr"
          style={ReactDOMRe.Style.make(
            ~borderRadius="5px",
            ~boxShadow="rgb(100, 85, 80) 0 0 120px -20px",
            //~color="#eef",
            ~marginTop="0",
            ~padding="0.5rem 0.8rem",
            (),
          )}>
          {{js|J'ai vecu au Cameroun pour dix ans et c'est de là que mon français vient. C'est mon pays d'origine et j'adore voyager.|js}
           |> str}
        </p>
        <h3
          lang="pt-br"
          style={ReactDOMRe.Style.make(
            ~fontWeight="400",
            ~margin="0 1rem 0 0",
            ~textAlign="right",
            (),
          )}>
          <span className="greenText"> {"Po" |> str} </span>
          <span className="yellowText"> {"rt" |> str} </span>
          <span className="blueText"> {"u" |> str} </span>
          <span className="yellowText"> {"gu" |> str} </span>
          <span className="greenText"> {{js|ês|js} |> str} </span>
        </h3>
        <p
          lang="pt-br"
          style={ReactDOMRe.Style.make(
            ~borderRadius="5px",
            ~boxShadow="rgb(100, 85, 80) 0 0 120px -20px",
            ~marginTop="0",
            ~padding="0.5rem 0.8rem",
            (),
          )}>
          {{js|Comecei a aprender o português em outubro de 2017. Estava escutando a música todos os dias e o app chamado |js}
           |> str}
          <a
            className="lightGreenText"
            href="https://www.duolingo.com"
            target="_blank">
            {"Duolingo" |> str}
          </a>
          {{js| me ajudou muito também. Ainda não estou completamente acostumado com o jeito de falar dos brasileiros (e doutros lusofones).|js}
           |> str}
        </p>
      </main>
      <Footer
        background="linear-gradient(to bottom, rgb(42, 33, 30), rgb(35, 28, 28), rgb(30, 20, 22))"
        boxShadow="rgb(25, 20, 15) 0px 0px 103px 10px"
        id="AboutFooter"
        height=""
        flexDirection="column"
        padding="1.5rem 6% 10rem"
        position="static"
      />
    </ScrollView>
  </div>;
};
