open Helpers;

let color = color =>
  ReactDOMRe.Style.make(
    ~color,
    ~display="inline-block",
    ~whiteSpace="pre-wrap",
    (),
  );

[@react.component]
let make = /*props*/
    (~onScroll: (int, unit) => unit, ~scrollTop: int) => {
  <>
    <Header pageName="miscellaneous" />
    <ScrollView id="ScrollView_Misc" onScroll scrollTop>
      <Box
        flexDirection="column"
        //height="100%"
        padding="2rem"
        justifyContent="center">
        <p
          style={ReactDOMRe.Style.make(
            ~margin="2rem auto",
            ~maxWidth="780px",
            ~padding="0 5%",
            (),
          )}>
          {"Front-end written in " |> str}
          <a
            href="https://reasonml.github.io/"
            style={color("#ff9090")}
            target="_blank">
            {"ReasonML" |> str}
          </a>
          <br />
          {"Icons from " |> str}
          <a
            href="https://feathericons.com/"
            style={color("#dbb")}
            target="_blank">
            {"Feather Icons" |> str}
          </a>
          <br />
          {"Free fonts from " |> str}
          <a
            href="https://www.fontsquirrel.com/"
            style={color("violet")}
            target="_blank">
            {"Font Squirrel" |> str}
          </a>
        </p>
      </Box>
    </ScrollView>
  </>;
};
