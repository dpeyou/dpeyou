open Helpers;
open Images;
open Projects;
open Types;

type state = {
  currentImg: screenShot,
  currentImgIndex: int,
  imgList: array(screenShot),
  showComponent: bool,
};

type action =
  | ChangeImg(
      array(screenShot),
      int /*index of the current image*/,
      direction /*whether the user clicked left or right. This is used to add to (+1), or subtracted from (-1) the index*/,
    )
  | LoadImgs(array(screenShot), int /*index of the current image*/)
  | ToggleComponent(bool);

[@react.component]
let make = /*props*/ (~onScroll: (int, unit) => unit, ~scrollTop: int) => {
  //---------------------------------
  //-----------------start of reducer
  let (state, dispatch) =
    React.useReducer(
      (state, action) =>
        switch (action) {
        | ChangeImg(imgs, index, direction) => {
            ...state,
            currentImg: {
              imgs[(
                     switch (direction) {
                     | Left => index + (Array.length(imgs) - 1)
                     | Right => index + 1
                     | _ => 0
                     }
                   )
                   mod Array.length(imgs)];
            },

            currentImgIndex:
              (
                switch (direction) {
                | Left => index + (Array.length(imgs) - 1)
                | Right => index + 1
                | _ => 0
                }
              )
              mod Array.length(imgs),
          }
        | LoadImgs(imgs, index) => {
            currentImg: {
              imgs[index];
            },
            currentImgIndex: index,
            imgList: imgs,
            showComponent: true,
          }
        | ToggleComponent(show) =>
          show == true
            ? {...state, showComponent: false}
            : {...state, showComponent: true}
        },
      //initial state
      {
        currentImg: {
          alt: "",
          src: "",
        },
        currentImgIndex: 0,
        imgList: [||],
        showComponent: false,
      },
    );
  //------------------------------------
  //----------------------end of reducer

  //shorthand
  let currentImg: screenShot = state.currentImg;

  //------------------------------------
  //------------------------------layout
  <div id="Page_Gallery">
    <Header pageName="gallery" />
    //-----------------------------
    //------------full screen image
    <Swipe
      onSwipedLeft={_event =>
        //open menu
        dispatch(ChangeImg(state.imgList, state.currentImgIndex, Right))}
      onSwipedRight={_event =>
        dispatch(ChangeImg(state.imgList, state.currentImgIndex, Left))
      }>
      <div
        className="zoomedImage"
        style={ReactDOMRe.Style.make(
          ~background="rgba(40, 30, 30, 0.875)",
          ~display={state.showComponent ? "flex" : "none"},
          ~height="100%",
          ~justifyContent="center",
          ~left="0",
          ~margin="0 auto",
          ~position="absolute",
          ~right="0",
          ~top="0",
          ~transition="300ms",
          ~zIndex="5",
          (),
        )}>
        //------------------------------
        //caption above the zoomed image

          <p style={ReactDOMRe.Style.make(~position="absolute", ~top="0", ())}>
            {state.currentImg.alt |> str}
          </p>
          //------------
          //zoomed image
          <img
            alt={currentImg.alt}
            src={currentImg.src}
            style={ReactDOMRe.Style.make(
              ~height="82%",
              ~position="absolute",
              ~top="3.4rem",
              (),
            )}
          />
          //----------------------------------------------------
          //left and right buttons for navigating through images
          <Box bottom="0" maxWidth="330px" position="absolute" width="70%">
            //left button

              <Button
                onClick={() =>
                  dispatch(
                    ChangeImg(state.imgList, state.currentImgIndex, Left),
                  )
                }>
                <img
                  src=leftArrow
                  style={ReactDOMRe.Style.make(
                    ~padding="1rem",
                    ~width="2.0rem",
                    (),
                  )}
                />
              </Button>
              //right button
              <Button
                onClick={() =>
                  dispatch(
                    ChangeImg(state.imgList, state.currentImgIndex, Right),
                  )
                }>
                <img
                  src=rightArrow
                  style={ReactDOMRe.Style.make(
                    ~padding="1rem",
                    ~width="2.0rem",
                    (),
                  )}
                />
              </Button>
              //close-zoomed-image button
              <Button
                fontSize="3rem"
                onClick={_event => dispatch(ToggleComponent(true))}>
                <img
                  src=chevronX
                  style={ReactDOMRe.Style.make(
                    ~padding="1rem",
                    ~width="2.0rem",
                    (),
                  )}
                />
              </Button>
            </Box>
        </div>
    </Swipe>
    //------------------------------------
    //------------end of full screen image
    //
    //-----------------------
    //-------------------body
    <ScrollView id="ScrollView_Gallery" onScroll scrollTop>
      <main
        style={ReactDOMRe.Style.make(
          ~margin="2rem auto 10rem",
          ~padding="0 6%",
          (),
        )}>
        <Box flexDirection="column" margin="4.5rem 0 4rem">
          //page title

            <h1
              style={ReactDOMRe.Style.make(
                ~color="#ffe0a0",
                ~margin="0",
                ~textAlign="center",
                (),
              )}>
              {"Gallery" |> str}
            </h1>
            <h3
              style={ReactDOMRe.Style.make(
                ~color="lightpink",
                ~margin="0",
                ~textAlign="center",
                (),
              )}>
              {"Project log" |> str}
            </h3>
          </Box>
        <p>
          {"Learning never stops, so I'm constantly updating these projects. Especially because I use them, allowing me to catch my mistakes."
           |> str}
        </p>
        <p>
          {"I have also made the source code for this website available on my Gitlab profile, the button for that is in the footer. Forgive me, as I have not yet added proper syntax-highlighting. Don't worry, it's coming."
           |> str}
        </p>
        <p> {"Most of my projects are deployed either via: " |> str} </p>
        <Box alignItems="flex-start" flexDirection="column" margin="1.5rem 0">
          <a
            href="https://zeit.co/"
            style={ReactDOMRe.Style.make(
              ~background=
                "linear-gradient(to top, rgb(70, 60, 63), rgb(80, 70, 63))",
              ~borderRadius="1px",
              ~color="#fee",
              ~margin="0.5rem",
              ~padding="0.8rem",
              ~textDecoration="none",
              (),
            )}
            target="_blank">
            {"Zeit HQ" |> str}
          </a>
          <a
            href="https://www.heroku.com/"
            style={ReactDOMRe.Style.make(
              ~background=
                "linear-gradient(to top, rgb(70, 60, 63), rgb(80, 70, 63))",
              ~borderRadius="1px",
              ~color="#Fee",
              ~margin="0.5rem",
              ~padding="0.8rem",
              ~textDecoration="none",
              (),
            )}
            target="_blank">
            {"Heroku" |> str}
          </a>
        </Box>
        <Box alignItems="flex-start" flexDirection="column">
          <h3
            className="yellowText"
            style={ReactDOMRe.Style.make(~lineHeight="1.65", ~margin="0", ())}>
            {"Most of these projects were made in a similar fashion:" |> str}
          </h3>
          <ul
            style={ReactDOMRe.Style.make(
              ~lineHeight="1.65",
              ~marginBottom="4rem",
              (),
            )}>
            <li>
              {"Written in ReasonML compiled to JavaScript. The Reason-React library (ReasonML bindings for ReactJS) will most likely be prominently featured."
               |> str}
            </li>
            <li>
              {"Presence of GraphQL through the URQL framework. Unfortunately Apollo-Client had a breaking change that I couldn't get around, so I migrated some apps to URQL."
               |> str}
            </li>
            <li> {"Bundled with ParcelJS over Webpack." |> str} </li>
          </ul>
        </Box>
        //---------------------------
        //---------------project list
        {projects
         |> Array.of_list
         |> Array.map((project: project) =>
              <Project
                description={project.description}
                key={project.name}
                onImgClick={(imgs, index, _event) =>
                  dispatch(LoadImgs(imgs, index))
                }
                name={project.name}
                screenShots={project.screenShots}
                sourceCode={project.sourceCode}
                url={project.url}
              />
            )
         |> React.array}
      </main>
      <Footer
        background="linear-gradient(to bottom, rgb(42, 33, 30), rgb(35, 28, 28), rgb(30, 20, 22))"
        boxShadow="rgb(25, 20, 15) 0px 0px 103px 10px"
        id="AboutFooter"
        height=""
        flexDirection="column"
        padding="1.5rem 6% 10rem"
        position="static"
      />
    </ScrollView>
  </div>;
};
