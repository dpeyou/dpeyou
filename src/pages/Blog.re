open Helpers;
open Posts;
open Types;

type state = {
  currentPost: post,
  isPostOpened: bool,
};

type action =
  | TogglePost(post, bool);

[@react.component]
let make = /*props*/ (~onScroll: (int, unit) => unit, ~scrollTop: int) => {
  //-------------------------------
  //---------------start of reducer
  let (state, dispatch) =
    React.useReducer(
      (_state, action) =>
        switch (action) {
        | TogglePost(post, bool) => {currentPost: post, isPostOpened: !bool}
        },
      //initial state
      {currentPost: introPost, isPostOpened: false},
    );
  //-------------------------------
  //----------------end of reducer

  //shorthand
  let currentPost: post = state.currentPost;
  let isPostOpened: bool = state.isPostOpened;

  //------------------------------------
  //------------------------------layout
  <div id="Page_Blog">
    //-----------------------
    //-------------------body

      {isPostOpened
         ? <Reader
             closePost={() =>
               dispatch(TogglePost(currentPost, isPostOpened))
             }
             currentPost
           />
         : <>
             <Header pageName="blog" />
             <ScrollView id="ScrollView_Blog" onScroll scrollTop>
               <main
                 style={ReactDOMRe.Style.make(
                   ~margin="2rem auto 10rem",
                   ~padding="0 6%",
                   (),
                 )}>
                 <Box flexDirection="column" margin="5.5rem 0 5rem">
                   /* <h1
                        style={ReactDOMRe.Style.make(
                          ~color="#ffe0a0",
                          ~margin="0",
                          ~textAlign="center",
                          (),
                        )}>
                        {"Idea dump" |> str}
                      </h1>*/

                     <h2
                       style={ReactDOMRe.Style.make(
                         ~color="#ffe0a0",
                         ~margin="0",
                         ~textAlign="center",
                         (),
                       )}>
                       {"Learning never stops" |> str}
                     </h2>
                   </Box>
                 //---------------------------
                 //-----------------blog posts
                 {posts
                  |> Array.of_list
                  |> Array.map((post: post) =>
                       <Post
                         clickPost={() =>
                           dispatch(TogglePost(post, isPostOpened))
                         }
                         content={post.content}
                         date={post.date}
                         key={post.id}
                         lastUpdate={post.lastUpdate}
                         id={post.id}
                         title={post.title}
                       />
                     )
                  |> React.array}
               </main>
               <Footer
                 background="linear-gradient(to bottom, rgb(42, 33, 30), rgb(35, 28, 28), rgb(30, 20, 22))"
                 boxShadow="rgb(25, 20, 15) 0px 0px 103px 10px"
                 id="AboutFooter"
                 height=""
                 flexDirection="column"
                 padding="1.5rem 6% 10rem"
                 position="static"
               />
             </ScrollView>
           </>}
    </div>;
};
